set -x EDITOR "kak"
set -x TERMINAL "dwm"
set -x BROWSER "firefox"
set -x READER "zathura"

set -x PATH "$HOME/bin:$PATH"
set -x XDG_CONFIG_HOME "$HOME/.config"

set fish_greeting
