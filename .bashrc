# .bashrc

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

set -o vi
alias kk='kak -c kakoune'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '
